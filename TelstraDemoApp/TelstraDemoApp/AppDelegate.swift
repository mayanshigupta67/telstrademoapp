//
//  AppDelegate.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        let viewController = ListViewController()
        //set up navigation controller
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.barTintColor = .yellow
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar
            .titleTextAttributes =
            [NSAttributedString.Key.font: UIDevice.current.userInterfaceIdiom
                == .pad ? UIFont.IPad.navTitle : UIFont.IPhone.navTitle]
        //set window root view controller
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
}

//
//  ListData.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation

struct List: Decodable {
    var mainTitle: String?
    var row: [Item]?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        mainTitle = try? values.decode(String.self, forKey: .maintitle)
        row = try? values.decode([Item].self, forKey: .row)
    }
}

struct Item: Decodable {
    var title: String?
    var description: String?
    var imageHref: String?
}

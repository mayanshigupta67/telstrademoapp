//
//  Constant.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
import UIKit

let network: NetworkManager = NetworkManager.sharedInstance

// MARK: Struct
struct Constants {
    // server URL
    static let dataSourceStr = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    //no internet connection
    static let kNoNetworkTitle = "No network"
    static let kNoInternetMsg = "Check your network settings and try again"
    // for server issue
    static let kErrorTitle = "An error occured"
    static let kserverIssueMsg = "Couldn't reach to the server"
    //alert view button
    static let kOKButtonTitle = "OK"
}

struct CellIdentifier {
    static let list = "listTableViewCell"
}

// MARK: Enums
enum CodingKeys: String, CodingKey {
    case maintitle = "title"
    case row = "rows"
}

enum ErrorResult: Error {
    case network
    case custom
}

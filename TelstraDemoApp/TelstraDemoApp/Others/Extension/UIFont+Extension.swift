//
//  UIFont+Extension.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 06/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
        struct IPhone {
            static let navTitle = UIFont.systemFont(ofSize: 14, weight: .semibold)
            static let title = UIFont(name: "HelveticaNeue-Bold", size: 14)
            static let desc = UIFont(name: "HelveticaNeue", size: 12)

        }
        struct IPad {
           static let navTitle = UIFont.systemFont(ofSize: 17, weight: .semibold)
            static let title = UIFont(name: "HelveticaNeue-Bold", size: 26)
            static let desc = UIFont(name: "HelveticaNeue", size: 20)
    }
}

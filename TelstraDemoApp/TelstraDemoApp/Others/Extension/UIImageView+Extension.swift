//
//  UIImageView+Extension.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    //download image
    func downloadAndSetImage(url: URL) {
        let processor = DownsamplingImageProcessor(size: CGSize(width: 100.0, height: 100.0))
        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
            placeholder: UIImage(named: "camera"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
        ]) { result in
            switch result {
            case .success(let value):
                dLog("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                dLog("Job failed: \(error.localizedDescription)")
            }
        }
    }

    //cancel downloading of an image
    func cancelDownload() {
        self.kf.cancelDownloadTask()
}
}

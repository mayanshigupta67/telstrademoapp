//
//  UIViewController+Extension.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 10/04/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func presentAlert(alertTitle: String, alertMessage: String) {
        let alert =  UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Constants.kOKButtonTitle, style: .destructive, handler: nil)
        alert.addAction(okAction)
        if !(self is UIAlertController) {
            self.present(alert, animated: true, completion: nil)
        }
    }

    //show no internet alert
    func showNoNetworkAlert() {
        self.presentAlert(alertTitle: Constants.kNoNetworkTitle, alertMessage: Constants.kNoInternetMsg)
    }
}

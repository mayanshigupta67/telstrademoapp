//
//  UIWindow+Extension.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 06/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {
    static var keyWindow: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}

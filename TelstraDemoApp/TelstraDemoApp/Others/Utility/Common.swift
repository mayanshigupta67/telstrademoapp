//
//  Common.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 06/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
import UIKit

// MARK: Show topmost controller in hierarchy
func topMostController() -> UIViewController? {
    guard let window = UIWindow.keyWindow,
        let rootViewController = window.rootViewController else {
        return nil
    }

    var topController = rootViewController
    if let navigationController = topController as? UINavigationController,
        let topViewController = navigationController.topViewController {
        topController = topViewController
    }
    while let newTopController = topController.presentedViewController {
        topController = newTopController
    }
    return topController
}

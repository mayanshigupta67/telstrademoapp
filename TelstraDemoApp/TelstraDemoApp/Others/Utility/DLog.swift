//
//  DLog.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 09/04/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
public func dLog(_ message: @autoclosure () -> String, filename: String = #file,
                 function: String = #function, line: Int = #line) {
#if DEBUG
print("[\(URL(string: filename)?.lastPathComponent ?? filename):\(line)]",
    "\(function)", message(), separator: " - ")
#else
#endif
}

//
//  NetworkManager.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 06/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import Foundation
import UIKit
import Reachability

class NetworkManager: NSObject {
    var reachability: Reachability?
    var isNetworkAvailable: Reachability.Connection?
    var navigationTitle: String?
    // Create a singleton instance
    static let sharedInstance: NetworkManager = { return NetworkManager() }()

    override init() {
        super.init()
        // Initialise reachability
        reachability = try? Reachability()
        isNetworkAvailable = reachability?.connection
        //enable network reachability
        enableReachability()
    }

    func enableReachability() {
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        do {
            // Start the network status notifier
            try reachability?.startNotifier()
        } catch {
            dLog("Unable to start notifier")
        }
    }

    @objc func networkStatusChanged(_ notification: Notification) {
        isNetworkAvailable = reachability?.connection
        reachability?.whenReachable = { reachability in
            dLog("Reachable via - \(reachability.connection)")
            self.hideNetworkBanner()
        }
        reachability?.whenUnreachable = { _ in
            dLog("Not reachable")
            self.showNoNetworkBanner()
        }
    }

    func showNoNetworkBanner() {
        navigationTitle = topMostController()?.navigationItem.title
        topMostController()?.navigationItem.title = Constants.kNoNetworkTitle
        topMostController()?.showNoNetworkAlert()
    }

    func hideNetworkBanner() {
        topMostController()?.dismiss(animated: false, completion: {
            topMostController()?.navigationItem.title = self.navigationTitle
        })
    }

    // remove observer and notifier
    deinit {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
}

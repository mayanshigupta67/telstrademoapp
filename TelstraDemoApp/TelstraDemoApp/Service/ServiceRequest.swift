//
//  ServiceRequest.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 06/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import UIKit

class ServiceRequest: NSObject {
    var task: URLSessionTask?
    var onErrorHandling: ((ErrorResult?) -> Void)?

    func loadSources(completion :@escaping (Result<List?, ErrorResult>) -> Void) {
        guard let requestURL = URL(string: Constants.dataSourceStr) else {
            onErrorHandling?(ErrorResult.custom)
            return
        }

        // cancel previous request if already in progress
        self.cancelFetchList()
        task = URLSession.shared.dataTask(with: requestURL) { data, _, error in
            if error != nil {
                completion(.failure(.custom))
                return
            }
            guard let responseData = data else { return }
            if let jsonData =  String(decoding: responseData, as: UTF8.self).data(using: .utf8) {
                do {
                    let decodedResponse = try JSONDecoder().decode(List.self, from: jsonData)
                    // we have good data – go back to the main thread
                    DispatchQueue.main.async {
                        completion(.success(decodedResponse))
                    }
                } catch let error {
                    print(error)
                }
            }
        }
        task?.resume()
    }

    func cancelFetchList() {
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}

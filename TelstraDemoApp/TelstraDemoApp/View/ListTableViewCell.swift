//
//  ListTableViewCell.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    private var imgURL: String?
    let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .top // .leading .firstBaseline .center .trailing .lastBaseline
        stackView.distribution = .equalCentering
        stackView.spacing = 4
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    let hStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center // .leading .firstBaseline .center .trailing .lastBaseline
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIDevice.current.userInterfaceIdiom == .pad ? UIFont.IPad.title : UIFont.IPhone.title
        return label
    }()
    lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIDevice.current.userInterfaceIdiom == .pad ? UIFont.IPad.desc : UIFont.IPhone.desc
        label.sizeToFit()
        return label
    }()

    var itemImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
        imageView.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        return imageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name:
        UIDevice.orientationDidChangeNotification, object: nil)
        // set UI
        addArrangeSubviews()
        setStackViewUI()
        setUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func rotated() {
         setUI()
    }

    fileprivate func addArrangeSubviews() {
        contentView.addSubview(hStackView)
        contentView.addSubview(itemImageView)
        vStackView.addArrangedSubview(titleLabel)
        vStackView.addArrangedSubview(descLabel)
        hStackView.addArrangedSubview(vStackView)
        hStackView.addArrangedSubview(itemImageView)
    }

    func setStackViewUI() {
        //set stack view constraints
        let marginGuide = contentView.layoutMarginsGuide
        let bottomConstraint = hStackView.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor)
        bottomConstraint.priority = .defaultLow
        bottomConstraint.isActive = true
        hStackView.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        hStackView.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        hStackView.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
    }

    //set constraint
    func setUI() {
        let screenWidth = UIScreen.main.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        //set imageview constraint
        let widthConstant = UIDevice.current.userInterfaceIdiom == .pad ? screenWidth * 0.08 : screenWidth * 0.15
        let imageViewWidthConstraint =
            NSLayoutConstraint(item: itemImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute:
                .notAnAttribute, multiplier: 1, constant: widthConstant)
        let imageViewHeightConstraint =
            NSLayoutConstraint(item: itemImageView,
            attribute: .height, relatedBy: .equal, toItem: nil,
            attribute: .notAnAttribute, multiplier: 1, constant: screenHeight * 0.15)
        itemImageView.addConstraints([imageViewWidthConstraint, imageViewHeightConstraint])
    }

    //called before cell is reused
    override func prepareForReuse() {
        super.prepareForReuse()
        itemImageView.cancelDownload()
        itemImageView.image = nil
    }
}

//
//  ListViewController.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    let tableView = UITableView()
    var safeArea: UILayoutGuide!
    lazy var blankImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit // image will never be strecthed vertially or horizontally
        imageView.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        imageView.isHidden = true
        return imageView
    }()
    private let refreshControl = UIRefreshControl()
    private var serviceRequest: ServiceRequest?
    var listViewModel: ListViewModel?
    let listCell: ListTableViewCell? = nil
    override func loadView() {
        super.loadView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.serviceRequest = ServiceRequest()
        if let serviceReq = self.serviceRequest {
            self.listViewModel = ListViewModel(serviceRequest: serviceReq)
        }
        getListData()
        updateUI()
    }
    private func updateUI() {
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        setupTableViewUI()
        setupInitialView()
    }
    //set up initial UI of table view
    func setupTableViewUI() {
        view.addSubview(tableView)
        view.addSubview(blankImageView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        //set constraint to table view
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            //image view constraints
            blankImageView.topAnchor.constraint(equalTo: tableView.topAnchor),
            blankImageView.leftAnchor.constraint(equalTo: tableView.leftAnchor),
            blankImageView.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            blankImageView.rightAnchor.constraint(equalTo: tableView.rightAnchor)
        ])
        blankImageView.image = UIImage(named: "Issue")
        tableView.separatorStyle = .none
        tableView.register(ListTableViewCell.self, forCellReuseIdentifier: CellIdentifier.list)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        // Add Refresh Control to Table View
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    func setupInitialView() {
        listViewModel?.setNavigationTitle = { [weak self] mainTitle in
            //update navigation title
            self?.setNavigationTitle(title: mainTitle)
        }
        listViewModel?.loadTableViewData = { [weak self] in
            //load table view
            self?.loadTableView()
        }
        //set error handling
        listViewModel?.onErrorHandling = { [weak self] error in
            //show error UI
            self?.updateErrorUI(error)
        }
    }
    //stop refreshing data
    fileprivate func stopRefreshingData() {
        if refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
    }
    fileprivate func showErrorAlert(_ error: ErrorResult?) {
        switch error {
        case .network:
            self.showNoNetworkAlert()
        default:
            self.presentAlert(alertTitle: Constants.kErrorTitle, alertMessage: Constants.kserverIssueMsg)
        }
    }
    fileprivate func updateErrorUI(_ error: ErrorResult?) {
        if self.tableView.visibleCells.isEmpty {
            self.blankImageView.isHidden = false
        }
        self.showErrorAlert(error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.stopRefreshingData()
        }
    }
    //refresh data
    @objc private func refreshData(_ sender: Any) {
        getListData()
    }
    fileprivate func getListData() {
        if network.isNetworkAvailable == .unavailable {
            updateErrorUI(ErrorResult.network)
        } else {
            // Fetch Data
            self.listViewModel?.populateData()
        }
    }
}

// MARK: view model callbacks
extension ListViewController {
    //set navigation title
    func setNavigationTitle(title: String) {
        self.navigationItem.title = title
    }
    //load table view data
    func loadTableView() {
        self.stopRefreshingData()
        self.blankImageView.isHidden = true
        self.tableView.separatorStyle = .singleLine
        self.tableView.reloadData()
    }
}

// MARK: Table view delegate,datasource
extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  listViewModel?.itemDataSource.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.list, for: indexPath)
            as? ListTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        guard let listData = listViewModel?.itemDataSource[indexPath.row] else { return cell }
        cell.titleLabel.text = listData.title
        cell.descLabel.text = listData.description
        cell.itemImageView.image = UIImage(named: "camera")
        // download image if image url is present
        if !(listData.imageHref?.isEmpty ?? true) {
            guard let imgUrl = URL(string: listData.imageHref ?? "") else { return cell }
            cell.itemImageView.downloadAndSetImage(url: imgUrl)
        }
        return cell
    }
}

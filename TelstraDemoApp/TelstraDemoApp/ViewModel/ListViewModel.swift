//
//  ListViewModel.swift
//  TelstraDemoApp
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import UIKit

class ListViewModel: NSObject {
    var setNavigationTitle: ((_:String) -> Void)?
    var loadTableViewData: (() -> Void)?
    var onErrorHandling: ((ErrorResult?) -> Void)?
    private var serviceRequest: ServiceRequest?
    var itemDataSource = [Item]() {
        didSet {
            self.loadTableViewData?()
        }
    }
    // init class
    init(serviceRequest: ServiceRequest) {
        self.serviceRequest = serviceRequest
    }
    // populate data
    func populateData() {
        self.serviceRequest?.loadSources { [unowned self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let listData) :
                    if let listData = listData {
                        self.setNavigationTitle?(listData.mainTitle ?? "")
                        if let items = listData.row?.filter({
                            $0.title != nil && $0.description != nil && $0.imageHref != nil }) {
                            if !items.isEmpty {
                                self.itemDataSource = items
                            }
                        }
                    }
                case .failure(let error) :
                    self.onErrorHandling?(error)
                }
            }
        }
    }
}

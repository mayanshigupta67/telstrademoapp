//
//  ListViewControllerTests.swift
//  TelstraDemoAppTests
//
//  Created by Mayanshi Gupta on 05/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import XCTest
@testable import TelstraDemoApp

class ListViewControllerTests: XCTestCase {
    var viewControllerUnderTest: ListViewController?
        var mockItem: [Item]?

        override func setUp() {
            super.setUp()
            viewControllerUnderTest = ListViewController()
            self.viewControllerUnderTest?.loadView()

            let bundle = Bundle(for: type(of: self))
            guard let url = bundle.url(forResource: "ListData", withExtension: "json") else {
                XCTFail("Missing file: ListData.json")
                return
            }

            guard let data = try? Data(contentsOf: url) else {
                XCTFail("Failed to create list")
                return
            }
            let decoder = JSONDecoder()
            guard let list = try? decoder.decode(List.self, from: data) else {
                XCTFail("list data is not in correct format")
                return
            }
            guard let item = list.row else {
                XCTFail("item data is not in correct format")
                return

            }
            mockItem = item
        }

        func testHasATableView() {
            XCTAssertNotNil(viewControllerUnderTest?.tableView)
        }

        func testTableViewHasDelegate() {
            XCTAssertNotNil(viewControllerUnderTest?.tableView.delegate)
        }

        func testTableViewConfromsToTableViewDelegateProtocol() {
            XCTAssertTrue((viewControllerUnderTest?.conforms(to: UITableViewDelegate.self))!)

        }

        func testTableViewHasDataSource() {
            XCTAssertNotNil(viewControllerUnderTest?.tableView.dataSource)
        }

        func testTableViewConformsToTableViewDataSourceProtocol() {
            XCTAssertTrue((viewControllerUnderTest?.conforms(to: UITableViewDataSource.self))!)
            XCTAssertTrue((viewControllerUnderTest?.responds(to: #selector(viewControllerUnderTest?.tableView(_:numberOfRowsInSection:))))!)
            XCTAssertTrue((viewControllerUnderTest?.responds(to: #selector(viewControllerUnderTest?.tableView(_:cellForRowAt:))))!)
        }

        func testNoOfRows() {
            guard let mockItem = mockItem else {
                XCTFail("Failed to create mock list")
                return
            }
            viewControllerUnderTest?.listViewModel?.itemDataSource = mockItem
            let tableView = MockListTableView()
            let numberOfRowsInSection = viewControllerUnderTest?.tableView(tableView, numberOfRowsInSection: 0)
            XCTAssertEqual(viewControllerUnderTest?.listViewModel?.itemDataSource.count, numberOfRowsInSection)
        }

        func testCellForRowAt() {
            guard let mockItem = mockItem else {
                XCTFail("Failed to create mock list")
                return
            }
            viewControllerUnderTest?.listViewModel?.itemDataSource = mockItem
            let indexPath = IndexPath(row: 0, section: 0)
            let tableView = MockListTableView()
            let tableViewCell = viewControllerUnderTest?.tableView(tableView, cellForRowAt: indexPath)
            guard let listTableViewCell = tableViewCell as? ListTableViewCell else {
                XCTFail("Failed to convert tableViewCell to ListTableViewCell")
                return
            }
            guard let title = listTableViewCell.titleLabel.text else {
                XCTFail("Failed to validate title")
                return
            }
            XCTAssertEqual(title, "Beavers")

            guard let desc = listTableViewCell.descLabel.text else {
                XCTFail("Failed to validate description")
                return
            }
            XCTAssertEqual(desc,
                           "Beavers are second only to humans in their ability to manipulate and change their environment. They can measure up to 1.3 metres long. A group of beavers is called a colony")
        }
    }

    class MockListTableView: UITableView {
        var customTableViewCell = ListTableViewCell()
        var tableViewCell = MockListTableViewCell()

        override func cellForRow(at indexPath: IndexPath) -> UITableViewCell? {
            return customTableViewCell
        }

        override func dequeueReusableCell(withIdentifier identifier: String) -> UITableViewCell? {
            return tableViewCell
        }

    }

    class MockListTableViewCell: ListTableViewCell {
        let mockTitle = UILabel()
        let mockDesc = UILabel()

        override var titleLabel: UILabel {
            get {
                return mockTitle
            }
            set {}
        }

        override var descLabel: UILabel {
            get {
                return mockDesc
            }
            set {}
    }
}

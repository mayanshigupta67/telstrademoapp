//
//  ServiceRequestTests.swift
//  TelstraDemoAppTests
//
//  Created by Mayanshi Gupta on 06/09/20.
//  Copyright © 2020 Telstra. All rights reserved.
//

import XCTest
@testable import TelstraDemoApp

class ServiceRequestTests: XCTestCase {
    
   func testData_whenSuccessfulResponseGiven()  throws {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "ListData", withExtension: "json") else {
            XCTFail("Missing file: ListData.json")
            return
        }

        let data = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        guard let list = try? decoder.decode(List.self, from: data) else {
            XCTFail("list data is not in correct format")
            return
        }
        guard let item = list.row else {
            XCTFail("item data is not in correct format")
            return

        }
        XCTAssertEqual(list.mainTitle, "About Canada")
        XCTAssertEqual(item[0].title, "Beavers")
        XCTAssertEqual(item[0].description,
                       "Beavers are second only to humans in their ability to manipulate and change their environment. They can measure up to 1.3 metres long. A group of beavers is called a colony")
    }
    
}
